//
//  MockBookService.swift
//  demoSwiftUI
//
//  Created by DoanDuyPhuong on 6/8/20.
//  Copyright © 2020 prox.com. All rights reserved.
//

import Foundation

class MockBookService: BookService {
    func bookList() -> [Book] {
        
    }
    
    // MARK: Mock data
    var books: [Book] = []

    var booksDetail: [BookDetail] = []

    var cart = Cart(items: [], numberOfItems: 0, total: 0)
    
    // MARK: Book details
    func bookDetails(bookId: Int) -> BookDetail {
        let details = booksDetail.first{ $0.bookId == bookId }
        return details!
    }

    func numberOfCartItems() -> Int {
        return cart.numberOfItems
    }

    func addToCart(bookId: Int) {
        guard let book = (books.first{ $0.id == bookId }) else { return }

        // Update cart
        cart.numberOfItems += 1
        cart.total += book.price
        updateItemCart(book: book)
    }

    // MARK: Cart
    func cartItems() -> Cart {
        return cart
    }

    func checkout() {
        // Checkout = empty cart item
        for item in cart.items {
            bookAvailable(id: item.item.id)
        }
        cart = Cart(items: [], numberOfItems: 0, total: 0)
    }
}
