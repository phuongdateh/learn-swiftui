//
//  BookDetail.swift
//  demoSwiftUI
//
//  Created by DoanDuyPhuong on 6/8/20.
//  Copyright © 2020 prox.com. All rights reserved.
//

import Foundation

struct BookDetail {
    var id: String
    var bookId: Int
    var author: String
    var title: String
    var price: Double
    var genre: [Genre]
    var kind: String
    var description: String
    var imageName: String
    var isAvailable: Bool
}
