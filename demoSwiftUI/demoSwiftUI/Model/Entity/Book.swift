//
//  Book.swift
//  demoSwiftUI
//
//  Created by DoanDuyPhuong on 6/8/20.
//  Copyright © 2020 prox.com. All rights reserved.
//

import Foundation

struct Book: Identifiable {
    var id: Int
    var title: String
    var author: String
    var price: Double
    var imageName: String
}
