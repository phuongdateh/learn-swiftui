//
//  Cart.swift
//  demoSwiftUI
//
//  Created by DoanDuyPhuong on 6/8/20.
//  Copyright © 2020 prox.com. All rights reserved.
//

import Foundation

struct Item: Identifiable {
    var id: String
    var item: Book
    var units: Int
}

struct Cart {
    var items: [Item]
    var numberOfItems: Int
    var total: Double
}
