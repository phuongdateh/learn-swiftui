//
//  BookService.swift
//  demoSwiftUI
//
//  Created by DoanDuyPhuong on 6/8/20.
//  Copyright © 2020 prox.com. All rights reserved.
//

import Foundation

protocol BookService {
    // Book list
    func bookList() -> [Book]

    // Book detail
    func bookDetails(bookId: Int) -> BookDetail
    func numberOfCartItems() -> Int
    func addToCart(bookId: Int)

    // Cart
    func cartItems() -> Cart
    func checkout()
}
